<!DOCTYPE html>
<html lang="id">

<?=$header;?>

<body>
  <div class="container-scroller">

<?=$navbar;?>

    <div class="container-fluid page-body-wrapper">

<?=$menu;?>

      <!-- page content start -->
      
      <!-- page content end -->
      
<?=$footer;?>

    </div>
  </div>

<?=$javascript;?>

</body>

</html>
